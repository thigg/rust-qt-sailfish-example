use std::env;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::process::Command;


fn install_mer_hacks() -> (String, bool) {
    let mer_sdk = match std::env::var("MERSDK").ok() {
        Some(path) => path,
        None => return ("".into(), false),
    };

    let mer_target = std::env::var("MER_TARGET")
        .ok()
        .unwrap_or("SailfishOS-latest".into());

    let arch = match &std::env::var("CARGO_CFG_TARGET_ARCH").unwrap() as &str {
        "arm" => "armv7hl",
        "i686" => "i486",
        "x86" => "i486",
        unsupported => panic!("Target {} is not supported for Mer", unsupported),
    };

    println!("cargo:rustc-cfg=feature=\"sailfish\"");

    let mer_target_root = format!("{}/targets/{}-{}", mer_sdk, mer_target, arch);

    let macos_lib_search = if cfg!(target_os = "macos") {
        "=framework"
    } else {
        ""
    };

    println!(
        "cargo:rustc-bin-link-arg=-rpath-link,{}/usr/lib",
        mer_target_root
    );
    println!(
        "cargo:rustc-bin-link-arg=-rpath-link,{}/lib",
        mer_target_root
    );

    println!(
        "cargo:rustc-link-search{}={}/toolings/{}/opt/cross/{}-meego-linux-gnueabi/lib",
        macos_lib_search, mer_sdk, mer_target, arch
    );

    println!(
        "cargo:rustc-link-search{}={}/usr/lib/qt5/qml/Nemo/Notifications/",
        macos_lib_search, mer_target_root
    );

    println!(
        "cargo:rustc-link-search{}={}/toolings/{}/opt/cross/lib/gcc/{}-meego-linux-gnueabi/4.9.4/",
        macos_lib_search, mer_sdk, mer_target, arch
    );

    println!(
        "cargo:rustc-link-search{}={}/usr/lib/",
        macos_lib_search, mer_target_root,
    );

    (mer_target_root, true)
}

fn main() {
    install_mer_hacks();
}